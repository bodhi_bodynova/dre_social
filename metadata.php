<?php
/**
 * Metadata version
 */
$sMetadataVersion = '2.0';

$aModule = [
    'id' => 'dre_social',
    'title' => [
        'en' => '<img src="../modules/bender/dre_social/out/img/favicon.ico" title="Bodynova Service Modul">odynova Social Modul',
        'de' => '<img src="../modules/bender/dre_social/out/img/favicon.ico" title="Bodynova Service Modul">odynova Social',
    ],
    'description' => [
        'en' => 'social scripts',
        'de' => 'Social Modul',
    ],
    'thumbnail' => 'out/img/logo_bodynova.png',
    'version' => '0.0.1',
    'author' => 'Bodynova GmbH',
    'url' => 'https://bodynova.de',
    'email' => 'support@bodynova.de',
    'controllers' => [
    ],
    'extend' => [
        \OxidEsales\Eshop\Core\ViewConfig::class => \Bender\dre_Social\Core\ViewConfig::class,
    ],
    'templates' => [
        'bender/dre_social/views/tpl/layout/footer__dd_footer_social_links.tpl' => 'bender/dre_social/views/tpl/layout/footer__dd_footer_social_links.tpl',
        'bender/dre_social/views/tpl/layout/base__base_style.tpl' => 'bender/dre_social/views/tpl/layout/base__base_style.tpl',
        'bender/dre_social/views/tpl/layout/base__base_js.tpl' => 'bender/dre_social/views/tpl/layout/base__base_js.tpl',
        'bender/dre_social/views/tpl/page/details/inc/productmain__details_productmain_productlinksselector.tpl' => 'bender/dre_social/views/tpl/page/details/inc/productmain__details_productmain_productlinksselector.tpl',
    ],
    'blocks' => [
        array(
        'template' => 'layout/footer.tpl',
        'block' => 'footer_social',
        'file' => '/views/blocks/layout/footer__dd_footer_social_links.tpl',
        ),
        array(
            'template' => 'layout/base.tpl',
            'block' => 'base_style',
            'file' => '/views/blocks/layout/base__base_style.tpl',
        ),
        array(
            'template' => 'layout/base.tpl',
            'block' => 'base_js',
            'file' => '/views/blocks/layout/base__base_js.tpl',
        ),
        array(
            'template' => 'page/details/inc/productmain.tpl',
            'block' => 'details_productmain_productlinksselector',
            'file' => '/views/blocks/page/details/inc/productmain__details_productmain_productlinksselector.tpl',
        ),
    ],
    'settings' => [
        
        array(
            'group' => 'dre-shariff_main',
            'name' => 'dre-shariff_main_with_jquery',
            'type' => 'bool',
            'value' => false,
        ),
        
        array(
            'group' => 'dre-shariff_main',
            'name' => 'dre-shariff_main_with_font_awesome',
            'type' => 'bool',
            'value' => false,
        ),
        array(
            'group' => 'dre-shariff_main',
            'name' => 'dre-shariff_main_data-info-url',
            'type' => 'str',
            'value' => 'http://ct.de/-2467514',
        ),
        array(
            'group' => 'dre-shariff_main',
            'name' => 'dre-shariff_main_data-info-display',
            'type' => 'select',
            'value' => 'blank',
            'constraints' => 'blank|popup|self',
            'position' => 0
        ),
        array(
            'group' => 'dre-shariff_main',
            'name' => 'dre-shariff_main_data-mail-url',
            'type' => 'str',
            'value' => '',
        ),
        array(
            'group' => 'dre-shariff_main',
            'name' => 'dre-shariff_main_data-mail-subject',
            'type' => 'str',
            'value' => '',
        ),
        array(
            'group' => 'dre-shariff_main',
            'name' => 'dre-shariff_main_data-mail-body',
            'type' => 'str',
            'value' => '{url}',
        ),
        array(
            'group' => 'dre-shariff_main',
            'name' => 'dre-shariff_main_data-services',
            'type' => 'str',
            'value' => 'facebook|twitter|linkedin|pinterest|xing|whatsapp|addthis|tumblr|diaspora|reddit|stumbleupon|threema|weibo|tencent-weibo|qzone|print|telegram|vk|flipboard|pocket|buffer|mail|info',
        ),

        array(
            'group' => 'dre-shariff_detail_page',
            'name' => 'dre-shariff_detail_page_display',
            'type' => 'bool',
            'value' => false,
        ),
        array(
            'group' => 'dre-shariff_detail_page',
            'name' => 'dre-shariff_detail_page_data-button-style',
            'type' => 'select',
            'value' => 'standard',
            /*'constraints' => 'standard|icon|icon-count',*/
            'constraints' => 'standard|icon',
            'position' => 0
        ),
        array(
            'group' => 'dre-shariff_detail_page',
            'name' => 'dre-shariff_detail_page_data-orientation',
            'type' => 'select',
            'value' => 'vertical',
            'constraints' => 'horizontal|vertical',
            'position' => 1
        ),
        array(
            'group' => 'dre-shariff_detail_page',
            'name' => 'dre-shariff_detail_page_data-theme',
            'type' => 'select',
            'value' => 'standard',
            'constraints' => 'standard|grey|white',
            'position' => 0
        ),

        array(
            'group' => 'dre-shariff_footer',
            'name' => 'dre-shariff_footer_display',
            'type' => 'bool',
            'value' => false,
        ),
        array(
            'group' => 'dre-shariff_footer',
            'name' => 'dre-shariff_footer_data-button-style',
            'type' => 'select',
            'value' => 'standard',
            /*'constraints' => 'standard|icon|icon-count',*/
            'constraints' => 'standard|icon',
            'position' => 0
        ),
        array(
            'group' => 'dre-shariff_footer',
            'name' => 'dre-shariff_footer_data-orientation',
            'type' => 'select',
            'value' => 'horizontal',
            'constraints' => 'horizontal|vertical',
            'position' => 0
        ),
        array(
            'group' => 'dre-shariff_footer',
            'name' => 'dre-shariff_footer_data-theme',
            'type' => 'select',
            'value' => 'standard',
            'constraints' => 'standard|grey|white',
            'position' => 0
        ),
    ],
];