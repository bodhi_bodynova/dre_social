[{assign var="oConfig" value=$oViewConf->getConfig()}]
[{assign var="bWithFontAwesome" value=$oConfig->getConfigParam('dre-shariff_with_font_awesome')}]
[{if $oViewConf->getTopActionClassName() == 'details'}]
    [{if $bWithFontAwesome=="1"}]
        [{oxstyle include=$oViewConf->getModuleUrl("dre_social", "out/src/shariff/shariff.complete.css")}]
    [{else}]
        [{oxstyle include=$oViewConf->getModuleUrl("dre_social", "out/src/shariff/shariff.min.css")}]
    [{/if}]
[{/if}]